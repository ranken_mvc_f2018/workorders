namespace WorkOrders.Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class increasedlengthoffilename : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Orders", "FormFileName", c => c.String(maxLength: 46));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "FormFileName", c => c.String(maxLength: 30));
        }
    }
}
