namespace WorkOrders.Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class increaselengthofimagefilename : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customers", "ImageName", c => c.String(maxLength: 50));
            AlterColumn("dbo.Orders", "FormFileName", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "FormFileName", c => c.String(maxLength: 46));
            AlterColumn("dbo.Customers", "ImageName", c => c.String(maxLength: 30));
        }
    }
}
