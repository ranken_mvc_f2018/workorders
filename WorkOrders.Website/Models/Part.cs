﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WorkOrders.Website.Models
{
    public class Part
    {
        [Key, Column(Order = 0)]
        [Required]
        public Guid OrderId { get; set; }
        [Key, Column(Order = 1)]
        [Required]
        public string PartNumber { get; set; }
        [Required]
        public string PartName { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required, DataType(DataType.Currency)]
        public decimal CostPerPart { get; set; }
        [Required]
        public bool IsLabor { get; set; }

        [NotMapped, DataType(DataType.Currency)]
        public decimal TotalCost
        {
            get { return Quantity * CostPerPart; }
        }

        // Navigation

        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
    }
}