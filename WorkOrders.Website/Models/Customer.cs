﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WorkOrders.Website.Models
{
    public class Customer
    {
        [Key]
        [Display(Name = "Customer #")]
        public Guid CustomerId { get; set; }

        [Display(Name = "Customer Name")]
        [Required(ErrorMessage = "Customer must have a name")]
        [StringLength(50, ErrorMessage = "The name you have is longer than 50 characters.")]
        [MinLength(3, ErrorMessage = "Your name must be at least 3 characters long.")]
        public string CustomerName { get; set; }

        [Display(Name = "Phone Number (w/ Area Code)")]
        [Required(ErrorMessage = "You must enter a phone number!!!")]
        [StringLength(14, ErrorMessage = "Phone number cannot be longer than 14 characters")]
        [MinLength(10, ErrorMessage = "Phone number must be at least 10 digits")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^(1-)?(\d{3}|\(\d{3}\) )-?\d{3}-?\d{4}$", 
            ErrorMessage = "Invalid phone number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Email Address")]
        [StringLength(200, ErrorMessage = "Email address cannot be longer than 200 characters")]
        [MinLength(6, ErrorMessage = "Email address must be at least 6 characters")]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        [Display(Name = "Street Address")]
        [StringLength(100)]
        [MinLength(3)]
        public string StreetAddress1 { get; set; }
        [Display(Name = "Apt/Suite")]
        [StringLength(10)]
        [MinLength(1)]
        public string StreetAddress2 { get; set; }
        [StringLength(30)]
        public string City { get; set; }
        [StringLength(2)]
        [MinLength(2)]
        public string State { get; set; }
        [Display(Name = "Zip Code")]
        [RegularExpression(@"^(\d{4}-)?\d{5}$", ErrorMessage = "Invalid zip code")]
        [StringLength(10)]
        [MinLength(5)]
        [DataType(DataType.PostalCode)]
        public string ZipCode { get; set; }

        [Display(Name = "Image")]
        [StringLength(50, ErrorMessage = "Image name has to be shorter than 50 characters.")]
        [DataType(DataType.ImageUrl)]
        public string ImageName { get; set; }

        // Navigation

        [ForeignKey("CustomerId")]
        public virtual ICollection<Order> Orders { get; set; } 
    }
}