﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WorkOrders.Website.Models;

namespace WorkOrders.Website.Controllers
{
    public class OrderController : Controller
    {
        private WorkOrdersDatabase db = new WorkOrdersDatabase();

        // GET: Order
        public ActionResult Index()
        {
            return View(
                db.Orders.Include("Customer").ToList()
            );
        }

        // GET: Order/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        private void PopulateDropDowns()
        {
            ViewBag.Customers = new SelectList(
                db.Customers.OrderBy(x => x.CustomerName),
                "CustomerId",
                "CustomerName"
            );
        }

        // GET: Order/Create
        public ActionResult Create()
        {
            PopulateDropDowns();
            return View();
        }

        // POST: Order/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OrderId,CustomerId,DateOfRepair,WrittenBy,Year,Make,Model,LicensePlate,Mileage,EstimateAmount,TotalLaborCost,TotalPartCost,TaxAmount,LUBE,OIL_CHANGE,FLUSH_TRANSMISSION,FLUSH_DIFFERENTIAL,WASH,POLISH")] Order order)
        {
            if (ModelState.IsValid)
            {
                order.OrderId = Guid.NewGuid();
                db.Orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            PopulateDropDowns();
            return View(order);
        }

        // GET: Order/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }

            PopulateDropDowns();
            return View(order);
        }

        // POST: Order/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "OrderId,CustomerId,DateOfRepair,WrittenBy,Year,Make,Model,LicensePlate,Mileage,EstimateAmount,TotalLaborCost,TotalPartCost,TaxAmount,LUBE,OIL_CHANGE,FLUSH_TRANSMISSION,FLUSH_DIFFERENTIAL,WASH,POLISH,FormFileName")]
            Order order,
            HttpPostedFileBase formUpload)
        {
            try
            {
                if (formUpload != null &&
                    ValidateFile("FormFileName", formUpload))
                {
                    UploadFile(order, formUpload);
                }

                if (ModelState.IsValid)
                {
                    db.Entry(order).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(
                    "FormFileName",
                    "Failed to save order!"
                );
            }

            PopulateDropDowns();
            return View(order);
        }

        private bool ValidateFile(string name, HttpPostedFileBase file)
        {
            if (file == null)
            {
                ModelState.AddModelError(name, "Please select a file.");
                return false;
            }
            if (file.ContentLength <= 0)
            {
                ModelState.AddModelError(name, "File size was zero.");
                return false;
            }
            if (file.ContentLength >= Constants.MAX_UPLOAD_FILE_SIZE)
            {
                ModelState.AddModelError(name, "File size was to big.");
                return false;
            }

            string fileExtension = Path.GetExtension(file.FileName).ToLower();
            if (!Constants.ALLOWED_FORM_EXTENSIONS.Contains(fileExtension))
            {
                ModelState.AddModelError(name, $"File extension {fileExtension} not allowed.");
                return false;
            }

            return true;
        }

        private void UploadFile(Order order, HttpPostedFileBase file)
        {
            string folderPath = Server.MapPath(Constants.ORDER_FOLDER_PATH);

            string fileExtension = Path.GetExtension(file.FileName).ToLower();
            string fileName = $"FORM_{order.OrderId}{fileExtension}";
            string filePath = Path.Combine(folderPath, fileName);
            file.SaveAs(filePath);
            order.FormFileName = fileName;
        }

        // GET: Order/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Order/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
