﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using WorkOrders.Website.Models;

namespace WorkOrders.Website.Controllers
{
    public class CustomerController : Controller
    {
        private WorkOrdersDatabase db = new WorkOrdersDatabase();

        // GET: Customer
        public ActionResult Index()
        {
            return View(
                db.Customers.OrderBy(x => x.CustomerName).ToList());
        }

        // GET: Customer/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        private void PopulateDropDowns()
        {
            string folderPath = Server.MapPath(Constants.CUSTOMER_FOLDER_PATH);
            ViewBag.Images = 
                Directory.EnumerateFiles(folderPath)
                         .Select(x => Path.GetFileName(x));
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            PopulateDropDowns();
            return View();
        }

        // POST: Customer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(Include = "CustomerId,ImageName,CustomerName,PhoneNumber,EmailAddress,StreetAddress1,StreetAddress2,City,State,ZipCode")]
            Customer customer,
            HttpPostedFileBase imageUpload)
        {
            customer.CustomerId = Guid.NewGuid();

            try
            {
                if (imageUpload != null &&
                    ValidateImage("ImageName", imageUpload))
                {
                    UploadCustomerImage(customer, imageUpload);
                }

                if (ModelState.IsValid)
                {
                    db.Customers.Add(customer);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(
                    "ImageName",
                    "Failed to save customer!"
                );
            }

            PopulateDropDowns();
            return View(customer);
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }

            PopulateDropDowns();
            return View(customer);
        }

        // POST: Customer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "CustomerId,ImageName,CustomerName,PhoneNumber,EmailAddress,StreetAddress1,StreetAddress2,City,State,ZipCode")]
            Customer customer,
            HttpPostedFileBase imageUpload)
        {
            try
            {
                if (imageUpload != null &&
                    ValidateImage("ImageName", imageUpload))
                {
                    UploadCustomerImage(customer, imageUpload);
                }

                if (ModelState.IsValid)
                {
                    db.Entry(customer).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(
                    "ImageName",
                    "Failed to save customer!"
                );
            }

            PopulateDropDowns();
            return View(customer);
        }

        private bool ValidateImage(string name, HttpPostedFileBase file)
        {
            if (file == null)
            {
                ModelState.AddModelError(name, "Please select a file.");
                return false;
            }
            if (file.ContentLength <= 0)
            {
                ModelState.AddModelError(name, "File size was zero.");
                return false;
            }
            if (file.ContentLength >= Constants.MAX_UPLOAD_FILE_SIZE)
            {
                ModelState.AddModelError(name, "File size was to big.");
                return false;
            }

            string fileExtension = Path.GetExtension(file.FileName).ToLower();
            if (!Constants.ALLOWED_IMAGE_EXTENSIONS.Contains(fileExtension))
            {
                ModelState.AddModelError(name, $"File extension {fileExtension} not allowed.");
                return false;
            }

            return true;
        }

        private void UploadCustomerImage(Customer customer, HttpPostedFileBase file)
        {
            // Resize the image
            WebImage img = new WebImage(file.InputStream);
            if (img.Width > Constants.CUSTOMER_IMG_MAX_WIDTH || img.Height > 600)
            {
                img.Resize(350, 600);
            }

            // Save the image
            string fileExtension = Path.GetExtension(file.FileName).ToLower();
            string fileName = $"CUSTOMER_{customer.CustomerId}{fileExtension}";
            img.Save(Constants.CUSTOMER_FOLDER_PATH + fileName);

            // Save the thumbnails
            img.Resize(100, 100);
            img.Save(Constants.CUSTOMER_FOLDER_PATH + Constants.THUMBNAILS_FOLDER + fileName);

            // Save the name of the image to the database
            customer.ImageName = fileName;
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Customer customer = db.Customers.Find(id);
            db.Customers.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
