﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WorkOrders.Website.Controllers
{
    public class UploadController : Controller
    {
        [HttpGet]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase myFile)
        {
            if (ValidateFile(myFile))
            {
                try
                {
                    UploadFile(myFile);
                    ViewBag.Message = "File Uploaded!";
                }
                catch (Exception)
                {
                    ModelState.AddModelError(
                        "myFile", 
                        "Sorry, an error occured with uploading the file, please try again.");
                }
            }
            return View();
        }

        private bool ValidateFile(HttpPostedFileBase file)
        {
            if (file == null)
            {
                ModelState.AddModelError("myFile", "Please select a file.");
                return false;
            }
            if (file.ContentLength <= 0)
            {
                ModelState.AddModelError("myFile", "File size was zero.");
                return false;
            }
            if (file.ContentLength >= 2000000)
            {
                ModelState.AddModelError("myFile", "File size was to big.");
                return false;
            }

            string fileExtension = Path.GetExtension(file.FileName).ToLower();
            string[] allowedExtensions = { ".pdf", ".png", ".jpg", ".jpeg" };
            if (!allowedExtensions.Contains(fileExtension))
            {
                ModelState.AddModelError("myFile", $"File extension {fileExtension} not allowed.");
                return false;
            }

            return true;
        }

        private void UploadFile(HttpPostedFileBase file)
        {
            string folderPath = Server.MapPath("~/Content/Uploads");
            string filePath = Path.Combine(folderPath, file.FileName);
            file.SaveAs(filePath);
        }
    }
}