namespace WorkOrders.Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class markingorderfieldsrequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Orders", "WrittenBy", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "Make", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "Model", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "LicensePlate", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "EstimateAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "EstimateAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Orders", "LicensePlate", c => c.String());
            AlterColumn("dbo.Orders", "Model", c => c.String());
            AlterColumn("dbo.Orders", "Make", c => c.String());
            AlterColumn("dbo.Orders", "WrittenBy", c => c.String());
        }
    }
}
