namespace WorkOrders.Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingforeignkeys : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Orders", "CustomerId");
            CreateIndex("dbo.Parts", "OrderId");
            AddForeignKey("dbo.Parts", "OrderId", "dbo.Orders", "OrderId", cascadeDelete: true);
            AddForeignKey("dbo.Orders", "CustomerId", "dbo.Customers", "CustomerId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.Parts", "OrderId", "dbo.Orders");
            DropIndex("dbo.Parts", new[] { "OrderId" });
            DropIndex("dbo.Orders", new[] { "CustomerId" });
        }
    }
}
