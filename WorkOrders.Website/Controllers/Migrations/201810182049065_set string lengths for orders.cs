namespace WorkOrders.Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class setstringlengthsfororders : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Orders", "WrittenBy", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Orders", "Make", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Orders", "Model", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Orders", "LicensePlate", c => c.String(nullable: false, maxLength: 8));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "LicensePlate", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "Model", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "Make", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "WrittenBy", c => c.String(nullable: false));
        }
    }
}
