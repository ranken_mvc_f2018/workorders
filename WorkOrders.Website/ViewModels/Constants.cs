﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkOrders.Website
{
    public static class Constants
    {
        // File and image uploads
        public const int MAX_UPLOAD_FILE_SIZE = 5 * 1024 * 1024;
        public const string THUMBNAILS_FOLDER = "Thumbnails/";
        public const int THUMBNAIL_SIZE = 100;

        public static readonly string[] ALLOWED_FORM_EXTENSIONS =
        {
            ".pdf", ".jpg", ".jpeg", ".docx", ".xlsx"
        };
        public static readonly string[] ALLOWED_IMAGE_EXTENSIONS =
        {
            ".jpg", ".jpeg", ".png"
        };

        // Customers
        public const string CUSTOMER_FOLDER_PATH = "~/Content/Customers/";
        public const int CUSTOMER_IMG_MAX_WIDTH = 350;
        public const int CUSTOMER_IMG_MAX_HEIGHT = 600;

        // Orders
        public const string ORDER_FOLDER_PATH = "~/Content/Orders/";
    }
}